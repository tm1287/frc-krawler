# FRC Krawler

**FRC Krawler** is a *FIRST* Robotics Scouting App by Team 2052 KnightKrawler forked by Team 1100.

# Project Management/Contributing
### Keeping up to date
  1. Check Team 2052's [Trello Board](https://trello.com/b/LCJL8AKc) for upcoming plans
  2. Look at the Milestones on the issues page
  
### Contributing
  1. Fork this repository, import into Android-Studio, and create a separate branch for your changes
  2. Make your changes, bug fixes, or new feature
  3. Submit a pull-request on this repository and we'll review and get it added
  
### Issues?
  If you were to have any issues with FRC Krawler, we want to have the issue fixed as soon as possible for you, but to do it in a timely manner, we need your effort in the situation too

  **Please provide the following in your issue:**
  1. A *detailed* description of what is happening
  2. An explanation on how to reproduce the issue



